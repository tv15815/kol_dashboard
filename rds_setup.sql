create table ctl_adapter_details
(adapter_id                 int(10),
connection_type            varchar(50),
source_name                varchar(50),
reference_payload_template varchar(100),
insert_by                  varchar(100),
insert_date                timestamp,
update_by                  varchar(100),
update_date                timestamp)


create table ctl_adapter_payload_details
(payload_id                int(10),
adapter_id                 int(10),
payload_details_file_name  varchar(50),
raw_file_dir               varchar(150),
structured_file_dir        varchar(150),
active_flag                varchar(10),
insert_by                  varchar(100),
insert_date                timestamp,
update_by                  varchar(100),
update_date                timestamp)


create table log_data_acquisition_smry
(cycle_id        varchar(20),
adapter_id      int(10),
payload_id      int(10),
source_name     varchar(50),
payload_details varchar(400),
load_type       varchar(50),
cluster_id      varchar(20),
status          varchar(20),
start_time      timestamp,
end_time        timestamp)

create table log_data_acquisition_dtl
(cycle_id            varchar(20),
adapter_id           int(10),
payload_id           int(10),
source_name          varchar(50),
step_name            varchar(50),
raw_file_dir         varchar(200),
structured_file_path varchar(200),
status               varchar(20),
start_time           timestamp,
end_time             timestamp)



insert into ctl_adapter_payload_details(6,4,'payload_trialtrove.json', 's3://aws-a0036-use1-00-d-s3b-rwcb-chb-data01/clinical-data-lake/dev/CITELINE/CITELINE_TRIALTROVE', 's3://aws-a0036-use1-00-d-s3b-rwcb-chb-data01/clinical-data-lake/dev/pre-ingestion/CITELINE/CITELINE_TRIALTROVE','Y', 'ss15038',now(),'ss15038',now())



insert into ctl_adapter_details where adapter_id
(4,'API', 'trialtrove', '{"disease_area_name": "Prostate Cancer","load_type": "full","disease_area_col_value": "Prostate Cancer"}','ss15038', now(),'ss15038',now())
