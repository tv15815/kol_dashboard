"""Module for MySQL Connection Management"""
#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ZS Associates'

######################################################Module Information###########################
#   Module Name         :   MySQLConnectionManager
#   Purpose             :   Enable MySQL connectivity. Enable other python modules to trigger
#                           queries on the configured
#                           MySQL instance
#   Input Parameters    :   NA
#   Output              :   NA
#   Execution Steps     :   Instantiate and object of this class and call the class functions
#   Predecessor module  :   This module is  a generic module
#   Successor module    :   NA
#   Pre-requisites      :   MySQL server should be up and running on the configured MySQL server
#   Last changed on     :   9 August 2018
#   Last changed by     :   Sushant Choudhary
#   Reason for change   :   Added method to decode the encoded password string
###################################################################################################

# Library and external modules declaration
import os
import sys
import logging
import traceback
import base64
from mysql.connector import connection
from ExecutionContext import ExecutionContext
from ConfigUtility import JsonConfigUtility
import CommonConstants as CommonConstants

sys.path.insert(0, os.getcwd())
#sys.path.insert(1, CommonConstants.AIRFLOW_CODE_PATH.__add__('/'))
# Define all module level constants here
MODULE_NAME = "MySQLConnectionManager"

##################class MySQLConnectionManager######################
# class contains all the functions related to MySQLConnectionManager
####################################################################


logger = logging.getLogger(__name__)
hndlr = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hndlr.setFormatter(formatter)


class MySQLConnectionManager(object):
    """Class for MySQL Connection Management"""
    # Instantiate an object of JsonConfigUtility class to load environment parameters json file
    def __init__(self, parent_execution_context=None):

        if parent_execution_context is None:
            self.execution_context = ExecutionContext()
        else:
            self.execution_context = parent_execution_context

        self.files = []
        self.execution_context = ExecutionContext()
        self.execution_context.set_context({"current_module": MODULE_NAME})
        self.configuration = JsonConfigUtility(CommonConstants.AIRFLOW_CODE_PATH + '/' +
                                               CommonConstants.ENVIRONMENT_CONFIG_FILE)
        self.connection = None

    # logger.info(self.configuration[CommonConstants.ENVIRONMENT_CONFIG_FILE])

    # This acts as the de-constructor for the objects of this class
    def __exit__(self, type, value, traceback):
        for file in self.files:
            os.unlink(file)

    ##################################################Execute my-sql query#########################
    # Purpose            :   Execute a query in MySQL
    # Input              :   Query String
    # Output             :   Tuple of tuples in case of a select query, None otherwise
    ###############################################################################################

    def execute_query_mysql(self, query, get_single_result=None):
        """ Module for executing queries"""
        mysql_connection = None
        try:
            status_message = "Starting function to execute a MySQLquery : "+str(query)
            logger.info(status_message, extra=self.execution_context.get_context())
            if query:
                query = query.strip()

            mysql_connection = self.get_my_sql_connection()
            cursor = mysql_connection.cursor(dictionary=True)
            status_message = "Created connection to MySQL"
            logger.debug(status_message, extra=self.execution_context.get_context())
            status_message = "Executing query on MySQL"
            logger.debug(status_message, extra=self.execution_context.get_context())
            if query.lower().startswith("select"):
                cursor.execute(query)
                if get_single_result:
                    result = cursor.fetchone()
                else:
                    result = cursor.fetchall()
            elif query.lower().startswith("insert into"):
                result = cursor.execute(query)
                if result != 0:
                    result = cursor.lastrowid
                cursor.execute("commit")
            else:
                cursor.execute(query)
                cursor.execute("commit")
                result = ""
            status_message = "Executed query on MySQL with result : " + str(result)
            logger.debug(status_message, extra=self.execution_context.get_context())
            return result
        except Exception as exception:
            error = "ERROR in " + self.execution_context.get_context_param("current_module") + \
                    " ERROR MESSAGE: " + str(traceback.format_exc())
            self.execution_context.set_context({"traceback": error})
            logger.error(error, extra=self.execution_context.get_context())
            raise exception

    def get_my_sql_connection(self):
        """Module for getting mySQL connection"""

        try:
            status_message = "Starting function to fetch my-sql connection"
            logger.info(status_message, extra=self.execution_context.get_context())

            # Preparing MySQL connection using the connection parameters provided in environment
            # parameters json file
            host = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                         "mysql_host"])
            user = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                         "mysql_username"])
            encoded_password = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY
                                                             , "mysql_password"])
            password = self.decrypt_value(encoded_password)
            port = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                         "mysql_port"])

            db = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                       "mysql_db"])
            if len(host) == 0 or len(user) == 0 or len(password) == 0 or len(db) == 0 \
                    or len(port) == 0:
                status_message = "Invalid configurations for " + MODULE_NAME + " in " + \
                                 CommonConstants.ENVIRONMENT_CONFIG_FILE
                logger.error(status_message, extra=self.execution_context.get_context())
                raise Exception
            mysql_connection = connection.MySQLConnection(host=host, user=user, passwd=password, db=db,
                                               port=int(port))
            status_message = "Connection to MySQL successful"
            logger.debug(status_message, extra=self.execution_context.get_context())
            return mysql_connection
        except Exception as exception:
            error = "ERROR in " + self.execution_context.get_context_param("current_module") + \
                    " ERROR MESSAGE: " + str(exception)
            self.execution_context.set_context({"traceback": error})
            logger.error(error, extra=self.execution_context.get_context())
            raise exception

    def decrypt_value(self, encoded_string):
        """
            Purpose :   This method decrypts encoded string
            Input   :   encoded value
            Output  :   decrypted value
        """
        try:
            status_message = "Started decoding for value:" + encoded_string
            logger.debug(status_message, extra=self.execution_context.get_context())
            decoded_string = base64.b64decode(encoded_string)
            status_message = "Completed decoding value:" + str(encoded_string)
            logger.info(status_message, extra=self.execution_context.get_context())
            return decoded_string
        except Exception as exception:
            status_message = "Error occured in decrypting value:" + str(encoded_string)
            error = "ERROR in " + self.execution_context.get_context_param("module_name") + \
                    " ERROR MESSAGE: " + str(traceback.format_exc())
            self.execution_context.set_context({"traceback": error})
            logger.error(status_message, extra=self.execution_context.get_context())
            raise exception
