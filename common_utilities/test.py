from MySQLConnectionManager import MySQLConnectionManager
import CommonConstants_bkp as CommonConstants
import sys
def get_adaptor_id(data_source):
    get_adapter_id_query_str = (
        "select adapter_id from {audit_db}.{adapter_details_table} "
        " where source_name = '{data_source}'"
    )
    get_payload_id_query = get_adapter_id_query_str.format(
        audit_db="audit_information",
        adapter_details_table=CommonConstants.ADAPTER_DETAILS_TABLE,
        data_source=data_source,
    )
    print(get_payload_id_query)
    get_payload_result = MySQLConnectionManager().execute_query_mysql(
        get_payload_id_query
    )
    adaptor_id = get_payload_result[0]["adapter_id"]
    print(type(adaptor_id),adaptor_id)
    return get_payload_result


data = get_adaptor_id("pubmed")
print(data)

