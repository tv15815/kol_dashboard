#!/bin/bash -x
#Script Name : emr_bootstrap_modified.sh as per Python 3
#Script Arguments : 1) S3 Bucket Name env-param.json 2) Cluster code path  commonconstant - emrcodepath
#Script Description : This script will install all the required python packages and applications
if [ -z "$1" ]
  then
    echo "Bucket name not supplied"
        exit 1
fi

if [ -z "$2" ]
  then
    echo "Cluster code path not supplied"
        exit 1
fi

bucket_name=$1
cluster_code_path=$2


sudo mkdir -p $cluster_code_path/code
sudo chmod 777 -R $cluster_code_path/code/
sudo aws s3 cp s3://$bucket_name/kol_poc/dev/code/code.tar.gz $cluster_code_path
sudo gunzip $cluster_code_path/code.tar.gz
sudo tar -xf $cluster_code_path/code.tar -C $cluster_code_path
sudo chmod 777 -R $cluster_code_path



#install mysql-connector

sudo /usr/bin/pip-3.4 install mysql-connector
sudo /usr/bin/pip-3.4 install awscli==1.16.140


#Install python logstash
sudo /usr/bin/pip-3.4 install python3-logstash
sudo yum install git -y
echo "done git install ---"
sudo mkdir /mnt/var/lib/hadoop/tmp/s3a
sudo chmod +777 /mnt/var/lib/hadoop/tmp/s3a


javapath="$(readlink -f $(which java))"

javapath=${javapath%/*}
javapath=${javapath%/*}
javapath=${javapath%/*}
export JAVA_HOME=$javapath
echo ' export PATH=$PATH:$JAVA_HOME/jre/bin '  >> ~/.bashrc

#Changes Added For Pyhive package used by Hive Table To S3 Utility
sudo /usr/bin/pip-3.4 install pyhive==0.5.2
sudo /usr/bin/pip-3.4 install thrift==0.11.0
sudo yum install cyrus-sasl-devel-2.1.23-13.16.amzn1 -y
sudo /usr/bin/pip-3.4 install sasl==0.2.1
sudo /usr/bin/pip-3.4 install thrift_sasl==0.3.0
sudo /usr/bin/pip-3.4 install pandas
sudo /usr/bin/pip-3.4 install pytz
sudo /usr/bin/pip-3.4 install xmltodict
sudo /usr/bin/pip-3.4 install urllib3==1.24.1
sudo /usr/bin/pip-3.4 install boto3==1.9.91
sudo /usr/bin/pip-3.4 install botocore==1.12.91
sudo /usr/bin/pip-3.4 install pymysql
sudo /usr/bin/pip-3.4 install jsonify


#changes made for installing s3-dist-cp for filetransfer utility
sudo aws s3 cp s3://$bucket_name/code/bootstrap/s3-dist-cp.tar.gz $cluster_code_path
sudo gunzip $cluster_code_path/s3-dist-cp.tar.gz
sudo tar -xf $cluster_code_path/s3-dist-cp.tar -C /usr/share/aws/emr



#Download the python script (install_packages.py) from S3
#Installs spark onto the slave nodes.
#sudo aws s3 cp s3://$bucket_name/code/bootstrap/install_packages.py $cluster_code_path
#sudo /usr/bin/python3.4 $cluster_code_path/install_packages.py $bucket_name $cluster_code_path > $cluster_code_path/install_packages.log

#cluster_id=$(cat /mnt/var/lib/info/job-flow.json | jq -r ".jobFlowId")
#echo $(aws emr describe-cluster --cluster-id $cluster_id) > /tmp/test.json

#cluster_detail=$(cat /tmp/test.json | jq '.Cluster.Status.State')

#if [ "$cluster_detail" == "WAITING" ]

#!/bin/bash -x

#cluster_id=$(jq ".jobFlowId" /emr/instance-controller/lib/info/job-flow.json | tr -d '"')
#master_ip=$(jq ".Cluster.MasterPublicDnsName" <(echo $(aws emr describe-cluster --cluster-id $cluster_id))| tr -d '"')
#sudo aws s3 cp s3://$bucket_name/python3/secondstage.sh /home/hadoop/secondstage.sh
#sudo bash /home/hadoop/secondstage.sh $bucket_name $master_ip & > output.log

#Install numpy and scipy and pyspark
sudo /usr/bin/pip-3.4 install numpy
sudo /usr/bin/pip-3.4 install scipy
sudo /usr/bin/pip-3.4 install pyspark

#sudo sed -i -e '$a\export PYSPARK_PYTHON=/usr/bin/python3' /etc/spark/conf/spark-env.sh


cd $cluster_code_path/code/

files_1="$(ls -1 -d $PWD/*.sh $PWD/*.conf $PWD/*.template $PWD/*.html $PWD/*.json  -p | tr '\n' ',')"

files=${files_1::-1}
echo "${files}"

touch /home/hadoop/temp_file
destdir_file=/home/hadoop/temp_file
echo "$files" >> "$destdir_file"
echo $(cat /home/hadoop/temp_file)


py_files_1="$(ls -1 -d $PWD/*.py  -p | tr '\n' ',')"

py_files=${py_files_1::-1}
echo "${py_files}"

touch /home/hadoop/temp_py
destdir_py=/home/hadoop/temp_py
echo "$py_files" >> "$destdir_py"
echo $(cat /home/hadoop/temp_py)
