select
   '$$disease_area$$' as disease_area_name,
   medlinecitation.pmid._value as `pmid`,
   medlinecitation.article.articletitle._value as `article_title`,
   medlinecitation.article.authorlist.author.initials as `initials`,
   medlinecitation.article.authorlist.author.lastname as `last_name`,
   medlinecitation.article.authorlist.author[0].affiliationinfo.affiliation as `affiliation`,
   medlinecitation.article.authorlist.author.forename as `first_name`,
   medlinecitation.medlinejournalinfo.country as `medlinejournalinfo_country`,
   pubmeddata.history.pubmedpubdate.day as `day`,
   pubmeddata.history.pubmedpubdate.month as `month`,
   pubmeddata.history.pubmedpubdate.year as `year`,
   pubmeddata.publicationstatus as `publicationstatus`,
   '{current_ts}' as load_timestamp
from
   $$table$$