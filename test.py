import pandas as pb
import pyarrow
data = pb.read_csv('pubmed4.csv')
dataframe = pb.DataFrame(columns = ['disease_area_name', 'pmid', 'article_title', 'initials',
'last_name', 'affiliation', 'first_name', 'medlinejournalinfo_country','day', 'month', 'year', 'publicationstatus', 'load_timestamp'])

for i in range(len(data)):
    disease_area_name = data.disease_area_name[i]
    pmid = data.pmid[i]
    article_title = data.article_title[i]
    medlinejournalinfo_country = data.medlinejournalinfo_country[i]
    publicationstatus = data.publicationstatus[i]
    load_timestamp = data.load_timestamp[i]
    affiliation = data.affiliation[i].strip('[]')
    initials = data.initials[i].strip('[]').replace(' ', '').split(',')
    day = data.day[i].strip('[]').replace(' ', '').split(',')
    month = data.month[i].strip('[]').replace(' ', '').split(',')
    year = data.year[i].strip('[]').replace(' ', '').split(',')
    last_name = data.last_name[i].strip('[]').replace(' ', '').split(',')
    first_name = data.first_name[i].strip('[]').replace(' ', '').split(',')
    for j in range(len(initials)):
        dataframe = dataframe.append({'pmid': pmid, 'article_title':article_title, 'last_name': last_name[j],
                                      'first_name': first_name[j], 'initials': initials[j], 'affiliation': affiliation,
                                      'medlinejournalinfo_country': medlinejournalinfo_country,
                                      'publicationstatus': publicationstatus, 'load_timestamp': load_timestamp,
                                      'day': day[j] if day[j] else None, 'month': month[j] if month[j] else None,
                                      'year': year[j] if year[j] else None}, ignore_index=True)
    dataframe.to_csv('pubmed4_converted.csv', index = False)